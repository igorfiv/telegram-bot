# -*- coding: utf-8 -*-

# import config
import telebot
from config import token
import requests, json, urllib

bot = telebot.TeleBot(token)


url = 'http://resources.finance.ua/ua/public/currency-cash.json'


def calculate_usd_parameters():
    data = json.loads(urllib.request.urlopen(url).read().decode())
    data_filtered = data['organizations']

    list_of_currencies = []
    for i in data_filtered:
        try:
            currency = i['currencies']['USD']['ask']
            list_of_currencies.append(currency)
        except KeyError:
            pass

    max_value = max(list_of_currencies)
    min_value = min(list_of_currencies)
    delta_value = max_value

    return max_value, min_value, delta_value


def calculate_eur_parameters():
    data = json.loads(urllib.request.urlopen(url).read().decode())
    data_filtered = data['organizations']

    list_of_currencies = []
    for i in data_filtered:
        try:
            currency = i['currencies']['EUR']['ask']
            list_of_currencies.append(currency)
        except KeyError:
            pass

    max_value = max(list_of_currencies)
    min_value = min(list_of_currencies)
    delta_value = max_value

    return max_value, min_value, delta_value


@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
    bot.reply_to(message, "Howdy, how are you doing?"
                          "Commands:"
                          "/help - show this info"
                          "/hello - show who write this bot")


@bot.message_handler(commands=['hello'])
def send_welcome(message):
    bot.reply_to(message, "Привет! Я новый тестовый бот.")


@bot.message_handler(commands=['currency_usd'])
def send_welcome(message):
    max_value, min_value, delta_value = calculate_usd_parameters()
    data = "Мінімальне значення: " + min_value + " Максимальне значення: " + max_value + " Дельта: " + delta_value
    bot.reply_to(message, data)


@bot.message_handler(func=lambda m: True)
def echo_all(message):
    bot.reply_to(message, message.text)


max_value, min_value, delta_value = calculate_usd_parameters()
print(max_value, min_value, delta_value)


max_value, min_value, delta_value = calculate_eur_parameters()
print(max_value, min_value, delta_value)


bot.polling()

# JSON-источник на украинском языке:
# http://resources.finance.ua/ua/public/currency-cash.json
# отправляем всем сообщение если курс гривны меняется более чем на 0.10